const Employee = require('../models/Employee.js')

const employeeCtrl = {}

employeeCtrl.getEmployees = async (req,res) => {
    const employees = await Employee.find();
    res.json(employees);
}

employeeCtrl.createEmployee = async (req,res) => {
    // console.log(req.body);
    const newEmployee = new Employee(req.body);
    await newEmployee.save();

    res.send({message: `Nuevo empleado ${newEmployee.name} creado exitosamente`});
}

employeeCtrl.getEmployee = async (req,res) => {
    const employee = await Employee.findById(req.params.id) ;
    res.send(employee);
}

employeeCtrl.editEmployee = async (req,res) => {
    await Employee.findByIdAndUpdate(req.params.id , req.body);
    res.json({status: "Employee UPDATED"});
}

employeeCtrl.deleteEmployee = async (req,res) => {
    await Employee.findByIdAndDelete(req.params.id);
    res.json({status: "Employee DELETED!"});
}

module.exports = employeeCtrl;