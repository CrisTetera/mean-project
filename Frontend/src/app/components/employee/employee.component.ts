import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { NgForm } from '@angular/forms'
import { Employee } from 'src/app/models/employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(public employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  resetForm(form: NgForm) {
    this.getEmployees();
    form.reset();
  }

  getEmployees() {
    this.employeeService.getEmployees().subscribe(
      (res) => { this.employeeService.employees = res },
      (err) => console.error(err)
    )
  }

  addEmployee(form: NgForm) {
    // console.log(form.value)
    if (form.value._id) {
      console.log('actualizando');
      this.employeeService.updateEmployee(form.value).subscribe(
        res => {
          this.getEmployees();
          form.reset();
        },
        err => console.error(err)
      )
    } else {
      this.employeeService.createEmployee(form.value).subscribe(
        res => {
          this.getEmployees();
          form.reset();
        },
        err => console.error(err)
      );   
    }
  }

  deleteEmployee(_id: string) {
    const res = confirm('Estás seguro de querer eliminar el empleado seleccionado?');
    if (res) {
      this.employeeService.deleteEmployee(_id).subscribe(
        res => {
          console.log(res);
          this.getEmployees();}, 
        err => console.error(err)
      );
    }
  }

  editEmployee(employee: Employee) {
    console.log(employee);
    this.employeeService.selectedEmployee = employee;
  }

}
